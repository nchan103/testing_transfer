# rgps_calval package

## Submodules

## rgps_calval.buoy_data module


### _class_ rgps_calval.buoy_data.HPoint(\*args, \*\*kwargs)
Bases: `shapely.geometry.point.Point`

Extends shapely Point class to be hashable
See: [https://stackoverflow.com/questions/52371968/merging-geodataframes-on-both-point-geometry-and-other-columns-at-the-same-time](https://stackoverflow.com/questions/52371968/merging-geodataframes-on-both-point-geometry-and-other-columns-at-the-same-time)


### _class_ rgps_calval.buoy_data.Hemisphere(value)
Bases: `enum.Enum`

An enumeration.


#### NORTH(_ = _ )

#### SOUTH(_ = _ )

### rgps_calval.buoy_data.build_buoy_gdf_web(buoy_url: str)

### rgps_calval.buoy_data.build_buoy_gdfs(buoy_file: str)
Use raw buoy data to build a gdf for that specfic buoy


### rgps_calval.buoy_data.build_daily_buoy_gdf_web(buoy_url: str)

### rgps_calval.buoy_data.build_daily_buoy_gdfs()
After running get_buoy_data, run this to build daily csv files for all buoy data in the RAW directory


### rgps_calval.buoy_data.build_hashable_buoy_gdf(buoy_file: str)
Builds a buoy GDF with hashable Point type


### rgps_calval.buoy_data.get_buoy_data(today=False)
Download all buoy data from the IABP database, use today=True for updating and today=False for downloading all files
Run from root directory to avoid creating data folders in the package.


* **Parameters**

    **today** – download only the data of buoys that were uploaded today



* **Returns**

    table of all the buoys with data that was downloaded with row format [file_name, datetime, file_size]



### rgps_calval.buoy_data.get_buoy_data_table(today: bool = True)
Get a table of all the names of buoys from the IABP database with 
row format [file_name, datetime, file_size]
You can download a buoy’s data from URL: ‘IABP_URL + file_name’


* **Parameters**

    **today** – download only the list of buoys that were uploaded today if true, otherwise download all buoys



* **Returns**

    table of all the buoys with row format [file_name, datetime, file_size]



### rgps_calval.buoy_data.get_buoy_data_table2(today: bool = True)
Get a table of all the names of buoys from the IABP database with
row format [file_name, datetime, file_size]
You can download a buoy’s data from URL: ‘IABP_URL + file_name’


* **Parameters**

    **today** – download only the list of buoys that were uploaded today if true, otherwise download all buoys



* **Returns**

    table of all the buoys with row format [file_name, datetime, file_size]



### rgps_calval.buoy_data.get_row_data(raw_row)
Create a data table row with [file_name, date, file_size] from the table in the IABP_URL index


* **Parameters**

    **raw_row** – unprocessed BS4 html object



* **Returns**

    data table row with [file_name, date, file_size]



### rgps_calval.buoy_data.insert_buoy_row_into_database(buoy_row, header_row)
Inserts a single buoy table row into the database


### rgps_calval.buoy_data.load_daily_buoy_gdfs(day: datetime.date)
Load all buoy data for a given day and return as gdf


### rgps_calval.buoy_data.load_daily_buoy_gdfs_web(day: datetime.date, hem: rgps_calval.buoy_data.Hemisphere)
load a gdf of daily buoy data from hemisphere from IABP website


* **Param**

    day: date for buoy data



* **Hem**

    Hemisphere.NORTH or Hemisphere.SOUTH


## rgps_calval.calval module


### _class_ rgps_calval.calval.Calval(shp_dir)
Bases: `object`

The Calval object is constructed with the directory for the RGPS sea ice vector shape files
to be evaluated and contains functions for plotting and measuring RGPS data against buoys


#### calval()

* **Returns**

    A full dataframe containing RMSE values for all files in the shp_dir. Shapefiles with no matches will have NaN for buoy and RMSE data.



#### find_rmse(shapefile)
This function takes in a shapefile and processes it using an interpolation/rmse algorithm.
RMSE is data is 3 values, (rms, x_error, and y_error)
rms = x
Calculate the RMSE errors for given shapefile against buoys that start and end on the given days


* **Parameters**

    **shapefile** – Shapefile name



* **Returns**

    list of RMSE values for all buoys matched to shapefiles, or [<shapefile> does not have any buoy matches’]



#### plot_calval(shapefile: str, plot_size: Optional[int] = None, selected_buoy: Optional[str] = None)
This function generates plots for the shapefile to visualize buoy and sea ice vector motion.


* **Parameters**

    
    * **shapefile** – filename of the shapefile that you want to process


    * **plot_size** – size of the plot, or None for default


    * **selected_buoy** – buoy to plot if found multiple buoys were matched



#### plot_interpolated_calval(shapefile: str, plot_size: Optional[int] = None)
This function generates plots for the shapefile to visualize buoy and sea ice vector motion.


* **Parameters**

    
    * **shapefile** – filename of the shapefile that you want to process


    * **plot_size** – size of the plot, or None for default


    * **selected_buoy** – buoy to plot if found multiple buoys were matched



#### read_sea_ice_data(shp_path)
Read in a RGPS sea ice vector shapefile from file path and project into to_epsg.
Also find start and end dates for the shapefile’s data


* **Parameters**

    **shp_path** – path to shapefile



* **Returns**

    ice_gdf_proj: Gdf of the RGPS sea ice movement vector field in the to_epsg projection



* **Returns**

    start_datetime: datetime of the first sea ice image



* **Returns**

    end_datetime: gdf of the second sea ice image



### rgps_calval.calval.calc_vectors(interpolated_buoy_xy, ice_xy)
This function finds the vector [x,y] of the interpolated (virtual) buoy movement and sea ice movement


* **Parameters**

    
    * **interpolated_buoy_xy** – interpolated buoy movement points


    * **ice_xy** – ice vector points



* **Returns**

    ice vector and buoy vector



### rgps_calval.calval.ckdnearest(point, gdf)

* **Parameters**

    
    * **point** – interpolated buoy point


    * **gdf** – sea ice gdf



* **Returns**

    the nearest point in the gdf to the buoy point



### rgps_calval.calval.compare_buoy_ice(intersection, ice_gdf_proj, start_datetime, end_datetime)
Calcuate the RMSE values from a buoy and RGPS sea ice vectors


* **Parameters**

    
    * **intersection** – Gdf containing all buoys points nearest to the start and end datetimes and within the sea ice vector field in the to_epsg projection


    * **ice_gdf_proj** – Gdf of the RGPS sea ice movement vector field in the to_epsg projection


    * **start_datetime** – datetime of the first RGPS ice image


    * **end_datetime** – datetime of the second RGPS ice image



* **Returns**

    (rms, rms_x, rms_y)



### rgps_calval.calval.find_buoy_matches(target_datetime, ice_gdf)
Find the nearest upper and lower time bounded buoy data points within the sea ice area


* **Parameters**

    
    * **target_datetime** – Datetime of the RGPS sea ice image


    * **ice_gdf** – Gdf of the RGPS sea ice movement vector field in the to_epsg projection



* **Returns**

    gdf containing the buoy data points that are within the ice_gdf area and were captured immediately before and after target_datetime



### rgps_calval.calval.get_nearest_times(target_datetime, gdf)
Get the two lines with the closest datetimes above and below the target datetime in
the given gdf with a datetime field


* **Parameters**

    
    * **target_datetime** – the target time


    * **gdf** – gdf with a datetime field



* **Returns**

    gdf containing only the line with datetime immediately before the target and the line with datetime immediately after the target



### rgps_calval.calval.interpolate_intersection(intersection, start_datetime, end_datetime)
geometry for linear interpolation between two buoy positions


* **Parameters**

    
    * **intersection** – geodataframe containing intersection of buoy and sea ice


    * **start_datetime** – start datetime of datetime object for sea ice gdf


    * **end_datetime** – end datetime of datetime object for sea ice gdf



* **Returns**

    start and end interpolated positions



### rgps_calval.calval.load_and_merge_daily_buoy_gdfs(target_date)
Load buoy points gdfs from the file system for the target_date and days
around target_date as necessary


* **Parameters**

    **target_date** – date of interest



* **Returns**

    gdf of buoys for the days around target_date



### rgps_calval.calval.make_convex_hull(ice_gdf_proj)
This function creates a geometry of the convex hull of the sea ice movement vector field


* **Parameters**

    **ice_gdf_proj** – Gdf of the RGPS sea ice movement vector field in the to_epsg projection



* **Returns**

    Gdf containing the convex hull geometry of the sea ice field in the to_epsg projection



### rgps_calval.calval.parse_filename(file)
Parses sea ice file name for data start and end datetimes


* **Parameters**

    **file** – shape file for processing



* **Returns**

    start datetime and end datetime datetime object



### rgps_calval.calval.plot_ice_and_buoy_polar(ice_polar, time_filtered_buoy, size=None)
This function generates a plot of the RGPS sea ice vector field and any buoys that were in the
location at the time of the images in the to_epsg projection. Sea ice vector field in red, buoy vector 
in green.


* **Parameters**

    
    * **ice_polar** – RGPS sea ice vector field in to_epsg


    * **time_filtered_buoy** – Buoys found by find_buoy_matches


    * **size** – size in meters to extend around the buoy regions of interest



* **Returns**

    None



### rgps_calval.calval.plot_ice_and_buoy_polar_interpolated(ice_polar, time_filtered_buoy, size=None)
This function generates a plot of the RGPS sea ice vector field and any buoys that were in the
location at the time of the images in the to_epsg projection. Sea ice vector field in red, buoy vector 
in green.


* **Parameters**

    
    * **ice_polar** – RGPS sea ice vector field in to_epsg


    * **time_filtered_buoy** – Buoys found by find_buoy_matches


    * **size** – size in meters to extend around the buoy regions of interest



* **Returns**

    None


## rgps_calval.sql module


### rgps_calval.sql.sql_get_buoy_data()
Connect to the PostgreSQL database server


### rgps_calval.sql.sql_insert_buoy_data(gdf: geopandas.geodataframe.GeoDataFrame)

### rgps_calval.sql.sql_insert_matching_buoy(matching_buoy_gdf: geopandas.geodataframe.GeoDataFrame)
Insert matching data from buoys to PostGIS database


### rgps_calval.sql.sql_insert_matching_ice_vec(matching_ice_gdf: geopandas.geodataframe.GeoDataFrame)
Insert matching ice vectors from buoys to PostGIS database

## rgps_calval.util module


### rgps_calval.util.build_daily_path(base_path: pathlib.Path, day: datetime.date, hemisphere: str, file_name: str = '', daily_directory: bool = False)
Create a path to an file/directory from the base_path that is organized by hemisphere, year, and month,
creating any subdirectories if they don’t exist
:param base_path: base directory to build directory structure in
:param day: date of the file or directory
:param hemisphere: hemisphere of interest
:param file_name: name of file to return, otherwise leave empty to return just the directory path
:param daily_directory: create a directory for every day of the year if True else only create directories per month
:return: path to local AMSR ice data file


### rgps_calval.util.daterange(start_date: datetime.date, end_date: datetime.date)
Creates a generator for a range of dates between the start date and end date (inclusive exclusive)
:param start_date: start date of the date range (included)
:param end_date: end date of the date range (excluded)
:return: date range generator object


### rgps_calval.util.fractional_doy_to_datetime(fdoy: float, year: int)

### rgps_calval.util.row_fractional_doy_to_datetime(row)

### rgps_calval.util.verify_dir(directory: pathlib.Path)
Returns directory path, creating the required directories if they don’t exist
:param directory: directory to verify existence of
:return: verified directory path

## Module contents
