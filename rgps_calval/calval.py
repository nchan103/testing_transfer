from glob import glob

from nbformat import current_nbformat
from rgps_calval.buoy_data import load_daily_buoy_gdfs
import numpy as np
import os
import pandas as pd
import geopandas as gpd
from datetime import datetime, timedelta
from shapely.geometry import Point
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from scipy.spatial import cKDTree
from tqdm.auto import tqdm

# Constants
polarstereo = ccrs.NorthPolarStereo()
to_epsg = polarstereo.proj4_init
from_epsg = '4326'


# change the directory to point to your local directory where the sea ice shapefiles are saved

def make_convex_hull(ice_gdf_proj):
    """
    This function creates a geometry of the convex hull of the sea ice movement vector field

    :param ice_gdf_proj: Gdf of the RGPS sea ice movement vector field in the to_epsg projection
    :return: Gdf containing the convex hull geometry of the sea ice field in the to_epsg projection
    """
    # Convert LineStrings into list of two points (start and end points)
    ice_gdf_proj['points'] = ice_gdf_proj.apply(lambda x: [y for y in x['geometry'].coords], axis=1)

    # create convex hull
    convex_hull = gpd.GeoSeries(
        [
            Point(tup) for tup_list in ice_gdf_proj['points'].to_list() for tup in tup_list
        ]
    ).unary_union.convex_hull

    # convert Shapely convex hull to gpd gdf
    ch_gdf = gpd.GeoDataFrame(geometry=gpd.GeoSeries(convex_hull))
    # reproject to match buoy projection
    ch_gdf_proj = ch_gdf.set_crs(to_epsg)
    return ch_gdf_proj


def plot_ice_and_buoy_polar(ice_polar, time_filtered_buoy, size=None):
    """
    This function generates a plot of the RGPS sea ice vector field and any buoys that were in the
    location at the time of the images in the to_epsg projection. Sea ice vector field in red, buoy vector 
    in green.

    :param ice_polar: RGPS sea ice vector field in to_epsg
    :param time_filtered_buoy: Buoys found by find_buoy_matches
    :param size: size in meters to extend around the buoy regions of interest
    :return: None
    """
    plt.figure()
    

    ax = plt.axes(projection=polarstereo)
    ax.set_title("BuoyID: " + str(time_filtered_buoy.BuoyID.iloc[0]))
    ax.stock_img()
    ax.coastlines()
    ax.gridlines(draw_labels=True)



    # Figure out minimum bounds to contain both DataFrames
    bb1 = time_filtered_buoy.total_bounds
    bb2 = ice_polar.total_bounds
    bb = [np.min([bb1[0], bb2[0]]), np.max([bb1[2], bb2[2]]),
          np.min([bb1[1], bb2[1]]), np.max([bb1[3], bb2[3]])]
    if size:
        bb = [bb1[0] - size, bb1[2] + size, bb1[1] - size, bb1[3] + size]

    def get_coords(geom):
        return list(geom.coords)

    coords = ice_polar.apply(lambda row: get_coords(row.geometry), axis=1)

    def get_first(row):
        return row[0]
    
    def get_second(row):
        return row[1]
    
    coords_x0, coords_y0, coords_x1, coords_y1 = \
        coords.apply(get_first).apply(get_first), \
        coords.apply(get_first).apply(get_second), \
        coords.apply(get_second).apply(get_first), \
        coords.apply(get_second).apply(get_second)
    coords_dx = coords_x1 - coords_x0
    coords_dy = coords_y1 - coords_y0

    buoy_coords = time_filtered_buoy.geometry
    buoy_x0, buoy_y0, buoy_x1, buoy_y1 = buoy_coords.iloc[0].x, buoy_coords.iloc[0].y, \
        buoy_coords.iloc[-1].x, buoy_coords.iloc[-1].y
    buoy_dx, buoy_dy = buoy_x1 - buoy_x0, buoy_y1 - buoy_y0
    head_width = abs(bb[1] - bb[0]) / 100
    ax.arrow(buoy_x0, buoy_y0, buoy_dx, buoy_dy, color='green', head_width=head_width)
    for x, y, dx, dy in zip(coords_x0, coords_y0, coords_dx, coords_dy):
        ax.arrow(x, y, dx, dy, color='red', head_width=head_width)
    ax.set_extent(bb, crs=polarstereo)  # This is an example of how to set the extent using bounds found above


def interpolate_intersection(intersection, start_datetime, end_datetime):
    """
    geometry for linear interpolation between two buoy positions

    :param intersection: geodataframe containing intersection of buoy and sea ice
    :param start_datetime: start datetime of datetime object for sea ice gdf
    :param end_datetime: end datetime of datetime object for sea ice gdf
    :return: start and end interpolated positions
    """

    def interpolate_buoy(zb1, zb2, tb1, tb2, ti0):
        """
        linear interpolation between two buoy positions

        :param zb2: buoy 2 position
        :param tb1: buoy 1 time
        :param tb2: buoy 2 time
        :param ti0: target time
        :return: buoy position at target time
        """

        dt = tb2 - tb1  # time difference
        vb = (np.array(zb2) - np.array(zb1)) / dt.total_seconds()
        tf = (ti0 - tb1).total_seconds()
        zb0 = zb1 + vb * tf  # estimate the buoy's position at ti0 via linear interpolation
        return zb0

    def get_timestamp(row):
        return datetime(row.Year - 1, 12, 31) + timedelta(days=row.DOY)

    row_start0 = intersection.iloc[0]
    row_start1 = intersection.iloc[1]
    row_end0 = intersection.iloc[-1]
    row_end1 = intersection.iloc[-2]

    row_start0xy = row_start0.geometry.coords[0]
    row_start1xy = row_start1.geometry.coords[0]
    row_end0xy = row_end0.geometry.coords[0]
    row_end1xy = row_end1.geometry.coords[0]

    row_start0t = get_timestamp(row_start0)
    row_start1t = get_timestamp(row_start1)
    row_end0t = get_timestamp(row_end0)
    row_end1t = get_timestamp(row_end1)

    start_int_xy = interpolate_buoy(row_start0xy, row_start1xy, row_start0t, row_start1t, start_datetime)
    end_int_xy = interpolate_buoy(row_end1xy, row_end0xy, row_end0t, row_end1t, end_datetime)

    return start_int_xy, end_int_xy


def ckdnearest(point, gdf):
    """
    :param point: interpolated buoy point
    :param gdf: sea ice gdf
    :return: the nearest point in the gdf to the buoy point
    """
    B = [np.array(geom.coords) for geom in gdf.geometry.to_list()]
    B = np.concatenate(B)
    ckd_tree = cKDTree(B)
    dist, idx = ckd_tree.query(point, k=1)
    return dist, idx // 2


def calc_vectors(interpolated_buoy_xy, ice_xy):
    """
    This function finds the vector [x,y] of the interpolated (virtual) buoy movement and sea ice movement

    :param interpolated_buoy_xy: interpolated buoy movement points
    :param ice_xy: ice vector points
    :return: ice vector and buoy vector
    """

    def make_vector(points):
        x_disp = points[1][0] - points[0][0]
        y_disp = points[1][1] - points[0][1]
        return [x_disp, y_disp]

    return make_vector(interpolated_buoy_xy), make_vector(ice_xy)


def compare_buoy_ice(intersection, ice_gdf_proj, start_datetime, end_datetime):
    """
    Calcuate the RMSE values from a buoy and RGPS sea ice vectors 

    :param intersection: Gdf containing all buoys points nearest to the start and end datetimes and within the sea ice vector field in the to_epsg projection
    :param ice_gdf_proj: Gdf of the RGPS sea ice movement vector field in the to_epsg projection
    :param start_datetime: datetime of the first RGPS ice image
    :param end_datetime: datetime of the second RGPS ice image
    :return: (rms, rms_x, rms_y, normal_rms_x, normal_rms_y)
    """
    interpolated_buoy_xy = interpolate_intersection(intersection, start_datetime, end_datetime)
    dist, nn_idx = ckdnearest(interpolated_buoy_xy[0], ice_gdf_proj)
    ice_xy = ice_gdf_proj.iloc[nn_idx].points
    buoy_vector, ice_vector = calc_vectors(interpolated_buoy_xy, ice_xy)
    est_buoy = interpolated_buoy_xy[0] + ice_vector  # estimate buoy position at end time using ice vector
    rms_x = abs(interpolated_buoy_xy[1][0] - est_buoy[0])
    rms_y = abs(interpolated_buoy_xy[1][1] - est_buoy[1])
    # take square root of mean squared error for rmse

    rms = np.sqrt(.5*(rms_x**2 + rms_y**2))
    
    time_difference = end_datetime - start_datetime
    time_difference_in_hours = time_difference.total_seconds() / 3600
    normal_rms_x = rms_x/time_difference_in_hours * 24
    normal_rms_y = rms_y/time_difference_in_hours * 24
    return rms, rms_x, rms_y, normal_rms_x, normal_rms_y



def load_and_merge_daily_buoy_gdfs(target_date):
    """
    Load buoy points gdfs from the file system for the target_date and days
    around target_date as necessary

    :param target_date: date of interest
    :return: gdf of buoys for the days around target_date
    """
    one_day = timedelta(days=1)
    buoy_gdf = load_daily_buoy_gdfs(target_date)
    if target_date.hour == 0 or target_date.hour == 1:
        prev_gdf = load_daily_buoy_gdfs(target_date - one_day)
        buoy_gdf = pd.concat([buoy_gdf, prev_gdf])
    elif target_date.hour == 23 or target_date.hour == 22:
        next_gdf = load_daily_buoy_gdfs(target_date + one_day)
        buoy_gdf = pd.concat([buoy_gdf, next_gdf])
    return buoy_gdf


def get_nearest_times(target_datetime, gdf):
    """
    Get the two lines with the closest datetimes above and below the target datetime in
    the given gdf with a datetime field

    :param target_datetime: the target time
    :param gdf: gdf with a datetime field
    :return: gdf containing only the line with datetime immediately before the target and the line with datetime immediately after the target
    """
    exactmatch = gdf[gdf.datetime == target_datetime]
    if not exactmatch.empty:
        return exactmatch
    else:
        lowerneighbor_ind = gdf[gdf.datetime < target_datetime].index.max()
        upperneighbor_ind = gdf[gdf.datetime > target_datetime].index.min()
        if np.isnan(lowerneighbor_ind) or np.isnan(upperneighbor_ind):
            return gpd.GeoDataFrame()
        return gdf.loc[[lowerneighbor_ind, upperneighbor_ind]]


def parse_filename(file):
    """
    Parses sea ice file name for data start and end datetimes

    :param file: shape file for processing
    :return: start datetime and end datetime datetime object
    """

    if os.sep in file:
        filename = os.path.basename(file)  # check to see if file path
    else:
        filename = file  # if just filename

    filename = os.path.splitext(filename)[0]  # drop the file extension
    start_str = filename.split('_')[0]
    end_str = filename.split('_')[1]
    start_datetime = datetime.strptime(start_str, '%y%m%d%H%M%S')
    end_datetime = datetime.strptime(end_str, '%y%m%d%H%M%S')
    # eyr = end_datetime.year  -- can piece out date elements, if needed
    return start_datetime, end_datetime


def find_buoy_matches(target_datetime, ice_gdf):
    """
    Find the nearest upper and lower time bounded buoy data points within the sea ice area

    :param target_datetime: Datetime of the RGPS sea ice image
    :param ice_gdf: Gdf of the RGPS sea ice movement vector field in the to_epsg projection
    :return: gdf containing the buoy data points that are within the ice_gdf area and were captured immediately before and after target_datetime
    """

    def find_spatial_intersection(buoy_gdf, ice_gdf):
        """
        Finds any buoys that are in the same area as the convex hull of the sea ice vectors

        :param buoy_gdf: Gdf of buoys in the to_epsg projection
        :param ice_gdf: Gdf of the RGPS sea ice movement vector field in the to_epsg projection
        :return: Gdf containing all buoys that are in the RGPS vector field. Filter by time next.
        """
        buoy_gdf['datetime'] = buoy_gdf.index
        ch_gdf_proj = make_convex_hull(ice_gdf)
        intersection = gpd.overlay(buoy_gdf, ch_gdf_proj, how='intersection')
        return intersection

    # start date - load daily buoy gdf according to parsed dates
    buoy_gdf = load_and_merge_daily_buoy_gdfs(target_datetime)
    

    # set and then reproject buoy gdf
    buoy_gdf_wgs = buoy_gdf.set_crs(epsg=from_epsg)
    buoy_gdf_proj = buoy_gdf_wgs.to_crs(to_epsg)
    # uncomment the next line to view gdf
    intersection = find_spatial_intersection(buoy_gdf_proj, ice_gdf)


    buoy_ids = list(set(intersection['BuoyID']))
    if not buoy_ids:
        return gpd.GeoDataFrame()
    time_intersection = gpd.GeoDataFrame()
    for buoy in buoy_ids:
        gdf_per_buoy = intersection[intersection['BuoyID'] == buoy]
        time_intersection_per_buoy = get_nearest_times(target_datetime, gdf_per_buoy)
        time_intersection = pd.concat([time_intersection, time_intersection_per_buoy])
    return time_intersection


class Calval:
    """
    The Calval object is constructed with the directory for the RGPS sea ice vector shape files
    to be evaluated and contains functions for plotting and measuring RGPS data against buoys
    """

    def __init__(self, shp_dir):
        self.shp_dir = shp_dir

    def read_sea_ice_data(self, shp_path):
        """
        Read in a RGPS sea ice vector shapefile from file path and project into to_epsg.
        Also find start and end dates for the shapefile's data

        :param shp_path: path to shapefile
        :return: ice_gdf_proj: Gdf of the RGPS sea ice movement vector field in the to_epsg projection
        :return: start_datetime: datetime of the first sea ice image
        :return: end_datetime: gdf of the second sea ice image
        """
        path = os.path.join(self.shp_dir, shp_path)
        ice_gdf = gpd.read_file(path)  # geometry is formatted as WKT - well-known text
        ice_gdf_wgs = ice_gdf.to_crs(epsg=from_epsg)
        # Project into North Polar Stereographic as set above
        ice_gdf_proj = ice_gdf_wgs.to_crs(to_epsg)
        start_datetime, end_datetime = parse_filename(shp_path)
        return ice_gdf_proj, start_datetime, end_datetime

    # how to account for if a buoy naturally doesn't move

    def find_rmse(self, shapefile):
        """
        This function takes in a shapefile and processes it using an interpolation/rmse algorithm.
        RMSE is data is 3 values, (rms, x_error, and y_error)
        rms = x
        Calculate the RMSE errors for given shapefile against buoys that start and end on the given days

        :param shapefile: Shapefile name
        :return: list of RMSE values for all buoys matched to shapefiles, or [<shapefile> does not have any buoy matches']
        """


        #make a new branch and a new notebook to find_rmse

        rmse_data_list = []
        ice_gdf, start_datetime, end_datetime = self.read_sea_ice_data(shapefile)

        #want to break find_bouy matches into smaller pieces
        #break it up into the matching in time vs matching in space 
        #each of time and space is its own cell
        #make     buoy_gdf = load_and_merge_daily_buoy_gdfs(target_datetime) its own cell
        #copy body of function of find_spacial_intersections into the cell
        #rest of function goes into a block for find match in time
        start_buoy_gdf = find_buoy_matches(start_datetime, ice_gdf)

        #we do it a 2nd time no need to break
        end_buoy_gdf = find_buoy_matches(end_datetime, ice_gdf)

        #
        if start_buoy_gdf.empty or end_buoy_gdf.empty:
            na = float('NaN')
            rmse_to_df = [(shapefile, na, na, na, na, na, na)]
        else:

            #finding buoys that are good matches in time and space at the beginning and end times
            start_buoy_ids = set(start_buoy_gdf['BuoyID'])
            end_buoy_ids = set(end_buoy_gdf['BuoyID'])
            buoy_matches = list(start_buoy_ids.intersection(end_buoy_ids))

            #
            merged_buoy_gdf = pd.concat([start_buoy_gdf, end_buoy_gdf])


            for buoy_id in buoy_matches:
                matched_buoy_gdf = merged_buoy_gdf[merged_buoy_gdf['BuoyID'] == buoy_id]

                #need to go into compare bouy ice to break down rmse calc
                #just copy paste the whole block of the function and put into a cell
                #if hit any classes then just copy paste the code
                rmse = compare_buoy_ice(matched_buoy_gdf, ice_gdf, start_datetime, end_datetime)
                rmse_data_list.append((shapefile, buoy_id, rmse))
                
            rmse_to_df = [(rmse[0], str(rmse[1]), rmse[2][0], rmse[2][1], rmse[2][2], rmse[2][3], rmse[2][4]) for rmse in rmse_data_list]
               
        return pd.DataFrame(rmse_to_df, columns=['shapefile', 'buoy', 'rms', 'x_error', 'y_error', 'normalized_x_error', 'normalized_y_error'])

    def plot_ice_and_buoy_polar(self, ice_polar, time_filtered_buoy, shapefile, plot_output_folder=None, size=None):
        """
        This function generates a plot of the RGPS sea ice vector field and any buoys that were in the
        location at the time of the images in the to_epsg projection. Sea ice vector field in red, buoy vector 
        in green.

        :param ice_polar: RGPS sea ice vector field in to_epsg
        :param time_filtered_buoy: Buoys found by find_buoy_matches
        :param size: size in meters to extend around the buoy regions of interest
        :return: None
        """
        plt.figure()

        ax = plt.axes(projection=polarstereo)
        ax.set_title("BuoyID: " + str(time_filtered_buoy.BuoyID.iloc[0]))
        ax.stock_img()
        ax.coastlines()
        ax.gridlines(draw_labels=True)



        # Figure out minimum bounds to contain both DataFrames
        bb1 = time_filtered_buoy.total_bounds
        bb2 = ice_polar.total_bounds
        bb = [np.min([bb1[0], bb2[0]]), np.max([bb1[2], bb2[2]]),
            np.min([bb1[1], bb2[1]]), np.max([bb1[3], bb2[3]])]
        if size:
            bb = [bb1[0] - size, bb1[2] + size, bb1[1] - size, bb1[3] + size]

        def get_coords(geom):
            return list(geom.coords)

        coords = ice_polar.apply(lambda row: get_coords(row.geometry), axis=1)

        def get_first(row):
            return row[0]
        
        def get_second(row):
            return row[1]
        
        coords_x0, coords_y0, coords_x1, coords_y1 = \
            coords.apply(get_first).apply(get_first), \
            coords.apply(get_first).apply(get_second), \
            coords.apply(get_second).apply(get_first), \
            coords.apply(get_second).apply(get_second)
        coords_dx = coords_x1 - coords_x0
        coords_dy = coords_y1 - coords_y0

        buoy_coords = time_filtered_buoy.geometry
        buoy_x0, buoy_y0, buoy_x1, buoy_y1 = buoy_coords.iloc[0].x, buoy_coords.iloc[0].y, \
            buoy_coords.iloc[-1].x, buoy_coords.iloc[-1].y
        buoy_dx, buoy_dy = buoy_x1 - buoy_x0, buoy_y1 - buoy_y0
        head_width = abs(bb[1] - bb[0]) / 100
        for x, y, dx, dy in zip(coords_x0, coords_y0, coords_dx, coords_dy):
            ax.arrow(x, y, dx, dy, color='red', head_width=head_width)
        ax.set_extent(bb, crs=polarstereo)  # This is an example of how to set the extent using bounds found above
        ax.arrow(buoy_x0, buoy_y0, buoy_dx, buoy_dy, color='green', head_width=head_width*2, label = f"rmse: {str(round(self.find_rmse(shapefile).rms[0],2))}")
        ax.legend()
        if plot_output_folder is not None and shapefile is not None:
            plt.savefig(plot_output_folder +shapefile[:-4] + '.pdf')  

    def plot_calval(self, shapefile: str, plot_size:int = None, selected_buoy:str = None, plot_output_folder=None ):
        #TODO EXPLAIN selected_buoy BETTER
        """
        This function generates plots for the shapefile to visualize buoy and sea ice vector motion.

        :param shapefile: filename of the shapefile that you want to process
        :param plot_size: size of the plot, or None for default
        :param selected_buoy: buoy to plot if found multiple buoys were matched
        """
        ice_gdf, start_datetime, end_datetime = self.read_sea_ice_data(shapefile)
        start_buoy_gdf = find_buoy_matches(start_datetime, ice_gdf)
        end_buoy_gdf = find_buoy_matches(end_datetime, ice_gdf)
        merged_buoy_gdf = pd.concat([start_buoy_gdf, end_buoy_gdf])

        #create a seperate gdf for each buoy incase multiple buoys were found
        buoys_id_list = merged_buoy_gdf.BuoyID.unique()
        id_to_gdf = dict()
        for id in buoys_id_list:
            id_to_gdf[id] = merged_buoy_gdf[merged_buoy_gdf.BuoyID == id]
        #if no buoy was selected just plot all ones matched
        if(selected_buoy == None):
            for id in buoys_id_list: 
                self.plot_ice_and_buoy_polar(ice_gdf, id_to_gdf[id], size=plot_size, shapefile=shapefile, plot_output_folder=plot_output_folder)
        #if buoy was selected just plot that one
        else:
            self.plot_ice_and_buoy_polar(ice_gdf, id_to_gdf[selected_buoy], size=plot_size, shapefile=shapefile, plot_output_folder=str(plot_output_folder))
        
    def bulk_plot_calval(self, rmse_list, size=None, plot_output_folder = None):
        for shapefile in rmse_list[~rmse_list["rms"].isnull()]["shapefile"]:
            self.plot_calval(shapefile, plot_size=size, plot_output_folder=str(plot_output_folder))
            

    def calval(self):
        """
        :return: A full dataframe containing RMSE values for all files in the shp_dir. Shapefiles with no matches will have NaN for buoy and RMSE data.
        """
        rmse_df_list = []
        sea_ice_results = [os.path.basename(path) for path in glob(self.shp_dir + '/*.shp')]
        for sea_ice_result in tqdm(sea_ice_results):
            result = self.find_rmse(sea_ice_result)
            rmse_df_list.append(result)
        return pd.concat(rmse_df_list)

    def plot_interpolated_calval(self, shapefile: str, plot_size:int = None):
        #TODO EXPLAIN selected_buoy BETTER
        """
        This function generates plots for the shapefile to visualize buoy and sea ice vector motion.
        
        
        :param shapefile: filename of the shapefile that you want to process
        :param plot_size: size of the plot, or None for default
        """
        ice_gdf, start_datetime, end_datetime = self.read_sea_ice_data(shapefile)
        start_buoy_gdf = find_buoy_matches(start_datetime, ice_gdf)
        end_buoy_gdf = find_buoy_matches(end_datetime, ice_gdf)
        merged_buoy_gdf = pd.concat([start_buoy_gdf, end_buoy_gdf])
        
        
        #loop through each buoy that was matched
        buoys_id_list = merged_buoy_gdf.BuoyID.unique()
        for id in buoys_id_list:
            current_buoy = merged_buoy_gdf[merged_buoy_gdf.BuoyID == id]
            interpolated_buoy = interpolate_intersection(current_buoy, start_datetime, end_datetime)
            print(interpolated_buoy)
            plot_ice_and_buoy_polar_interpolated(ice_gdf, interpolated_buoy, size=plot_size)
        #if no buoy was selected just plot all ones matched
        

        
       

def plot_ice_and_buoy_polar_interpolated(ice_polar, time_filtered_buoy, size=None):
    """
    This function generates a plot of the RGPS sea ice vector field and any buoys that were in the
    location at the time of the images in the to_epsg projection. Sea ice vector field in red, buoy vector 
    in green.
    Only used in plot_interpolated_calval
    
    :param ice_polar: RGPS sea ice vector field in to_epsg
    :param time_filtered_buoy: Buoys found by find_buoy_matches
    :param size: size in meters to extend around the buoy regions of interest
    :return: None
    """
    plt.figure()

    ax = plt.axes(projection=polarstereo)



    #ax.set_title("BuoyID: " + str(time_filtered_buoy.BuoyID.iloc[0]))
    ax.stock_img()
    ax.coastlines()
    ax.gridlines(draw_labels=True)


    # Figure out minimum bounds to contain both DataFrames
    bb1 = min(time_filtered_buoy[0][0], time_filtered_buoy[1][0]), \
        min(time_filtered_buoy[0][1], time_filtered_buoy[1][1]), \
        max(time_filtered_buoy[0][0], time_filtered_buoy[1][0]),\
        max(time_filtered_buoy[0][1], time_filtered_buoy[1][1])

    bb2 = ice_polar.total_bounds
    bb = [np.min([bb1[0], bb2[0]]), np.max([bb1[2], bb2[2]]),
        np.min([bb1[1], bb2[1]]), np.max([bb1[3], bb2[3]])]
    if size:
        bb = [bb1[0] - size, bb1[2] + size, bb1[1] - size, bb1[3] + size]

    def get_coords(geom):
        return list(geom.coords)

    coords = ice_polar.apply(lambda row: get_coords(row.geometry), axis=1)

    def get_first(row):
        return row[0]

    def get_second(row):
        return row[1]

    coords_x0, coords_y0, coords_x1, coords_y1 = \
    coords.apply(get_first).apply(get_first), \
    coords.apply(get_first).apply(get_second), \
    coords.apply(get_second).apply(get_first), \
    coords.apply(get_second).apply(get_second)
    coords_dx = coords_x1 - coords_x0
    coords_dy = coords_y1 - coords_y0

    #buoy_coords = time_filtered_buoy.geometry
    buoy_x0 = time_filtered_buoy[0][0]
    buoy_y0 = time_filtered_buoy[0][1]
    buoy_x1 = time_filtered_buoy[1][0]
    buoy_y1 = time_filtered_buoy[1][1]
    #buoy_x0, buoy_y0, buoy_x1, buoy_y1 = buoy_coords.iloc[0].x, buoy_coords.iloc[0].y, \
    #    buoy_coords.iloc[-1].x, buoy_coords.iloc[-1].y
    buoy_dx, buoy_dy = buoy_x1 - buoy_x0, buoy_y1 - buoy_y0
    head_width = abs(bb[1] - bb[0]) / 100
    ax.arrow(buoy_x0, buoy_y0, buoy_dx, buoy_dy, color='green', head_width=head_width)
    for x, y, dx, dy in zip(coords_x0, coords_y0, coords_dx, coords_dy):
        ax.arrow(x, y, dx, dy, color='red', head_width=head_width)
    ax.set_extent(bb, crs=polarstereo)  # This is an example of how to set the extent using bounds found above

def plot_error_hist(error_values, x_label = None, y_label = None, title = None):
    """
    plots x_error, y_error, or rms values from calval rmse calculations

    :param error_values: a pandas series containing y error or x error
    :type error_values: pandas series
    :param x_label: label for x axis, defaults to error_values series name
    :type x_label: str, optional
    :param y_label: label for y axis, defaults to Frequency
    :type y_label: str, optional
    :param title: title of plot, defaults to None
    :type title: str, optional
    """
    
    value_plotted = error_values.name
    legend = f"number_buoys: {error_values.size} mean: {round(error_values.mean(),2)} std_dev: {round(error_values.std(),2)}"
    ax = error_values.plot.hist(bins=50, label = legend)
    ax.legend()
    if x_label is not None:
        ax.set_xlabel(x_label)
    else:
        if value_plotted == "x_error":
            ax.set_xlabel("x-offset (m)")
        elif value_plotted == "y_error":
            ax.set_xlabel("y-offset (m)")
        elif value_plotted == "rms":
            ax.set_xlabel("RMSE (m)")
        else:
            ax.set_xlabel(str(value_plotted) +" (m)" )
    if y_label is not None:
        ax.set_ylabel(y_label)
    if title is not None:
        ax.set_title(title)
     



def calculate_summary_statistics(rmse_list):
    """
    plots the x_error, y_error, and RMSE of data from calval.calval
    as a box and whisker plot. Also generates a df of each error type's
    mean median and standard deviation


    :param rmse_list: a data frame consisting of columns named x_error, y_error, and rms
    :type rmse_list: pandas.DataFrame
    :return: return a DataFrame with mean median and mode for each error type
    :rtype: pandas.DataFrame
    """

    rmse_list = rmse_list[~rmse_list.buoy.isna()]

    #plot_error_vals
    rmse_list[["x_error", "y_error", "rms"]].boxplot()
    
    mean = []
    median = []
    std_dev = []
    rmse_keys = ["x_error", "y_error", "rms"]

    for key in rmse_keys:
        column = rmse_list[key]
        mean.append(column.mean())
        median.append(column.median())
        std_dev.append(column.std())
    data = {
    "Mean":mean,
    "Median":median,
    "Standard deviation":std_dev
    }
    summary_df = pd.DataFrame(data, index=["X error", "Y error", "RMSE"])
    return summary_df

