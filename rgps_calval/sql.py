from geopandas import GeoDataFrame
from sqlalchemy import create_engine



# What additional constraints do we need to specify the desired buoys?
# https://postgis.net/docs/ST_MakePolygon.html Do we actually need a polygon or is a bounding box good enough
FETCH_SQL = """
    SELECT * FROM buoy_data WHERE coordinate @
    ST_MakeEnvelope (
    $(xmin)s, $(ymin)s, 
    $(xmax)s, $(ymax)s 
    ) 
    AND 
    data_time BETWEEN $(starttime)s AND $(endtime)s
"""

db_connection_url = "postgresql://postgres:admin@localhost:5432/postgres"
con = create_engine(db_connection_url)


def sql_get_buoy_data():
    """ Connect to the PostgreSQL database server """

    gdf = GeoDataFrame.from_postgis(FETCH_SQL, con, geom_col="coordinate", crs="EPSG:4326")
    return gdf


def sql_insert_buoy_data(gdf: GeoDataFrame):
    gdf.to_postgis('web_buoy_data', con, if_exists='append')


def sql_insert_matching_buoy(matching_buoy_gdf: GeoDataFrame):
    """Insert matching data from buoys to PostGIS database"""
    matching_buoy_gdf.to_postgis('matching_buoy_data', con, if_exists='append')


def sql_insert_matching_ice_vec(matching_ice_gdf: GeoDataFrame):
    """Insert matching ice vectors from buoys to PostGIS database"""
    matching_ice_gdf.to_postgis('matching_buoy_data', con, if_exists='append')


if __name__ == '__main__':
    sql_get_buoy_data()
