from datetime import date, timedelta, datetime
from pathlib import Path
from typing import Generator
import os


def daterange(start_date: date, end_date: date) -> Generator[date, None, None]:
    """
    Creates a generator for a range of dates between the start date and end date (inclusive exclusive)
    :param start_date: start date of the date range (included)
    :param end_date: end date of the date range (excluded)
    :return: date range generator object
    """
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def build_daily_path(base_path: Path, day: date, hemisphere: str,
                     file_name: str = '', daily_directory: bool = False) -> Path:
    """
    Create a path to an file/directory from the base_path that is organized by hemisphere, year, and month,
    creating any subdirectories if they don't exist
    :param base_path: base directory to build directory structure in
    :param day: date of the file or directory
    :param hemisphere: hemisphere of interest
    :param file_name: name of file to return, otherwise leave empty to return just the directory path
    :param daily_directory: create a directory for every day of the year if True else only create directories per month
    :return: path to local AMSR ice data file
    """

    month_abbr = day.strftime("%b").lower()
    path_hemisphere = base_path/hemisphere
    path_year = path_hemisphere/str(day.year)
    path_month = path_year/month_abbr
    if not daily_directory:
        verify_dir(path_month)
        final_dir = path_month
    else:
        path_day = path_month/str(day.day)
        verify_dir(path_day)
        final_dir = path_day
    destination_item = final_dir/file_name
    return destination_item


def verify_dir(directory: Path) -> Path:
    """
    Returns directory path, creating the required directories if they don't exist
    :param directory: directory to verify existence of
    :return: verified directory path
    """
    if not directory.exists():
        os.makedirs(directory, exist_ok=True)
    return directory


def fractional_doy_to_datetime(fdoy: float, year: int):

    
    try:
        d = datetime(year=year, month=1, day=1)
        d += timedelta(fdoy)
        return d
    except (ValueError, OverflowError) as error:
        print(error)
        print('fdoy, year:', fdoy, year)
        d = datetime(999, 1, 1)
        return d



def row_fractional_doy_to_datetime(row):
    return fractional_doy_to_datetime(row.DOY, row.Year)
