import sys


sys.path.insert(0,'/Users/nathanc/Documents/Code/RGPS-CalVal/')

import requests
from requests.packages import urllib3
from pathlib import Path
from shapely.wkt import loads
from rgps_calval.util import verify_dir, row_fractional_doy_to_datetime, build_daily_path
from bs4 import BeautifulSoup
from datetime import datetime, date
import geopandas as gpd
import pandas as pd
from tqdm import tqdm
import shapely
from typing import List
from enum import Enum
import os

class Hemisphere(Enum):
    NORTH = 1
    SOUTH = 2


pd.options.display.width = 0
pd.options.display.max_colwidth = 150
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Paths
module_path = os.path.abspath(__file__)
module_dir = Path(os.path.dirname(module_path))
DATA_DIR = module_dir.parent / 'calval_data'
BUOY_DIR = DATA_DIR / 'buoy_data'
RAW_BUOY_DIR = BUOY_DIR / 'raw'
GDF_BUOY_DIR = BUOY_DIR / 'gdf'
IABP_URL = 'https://iabp.apl.uw.edu/WebData/'
IABP_DAILY_URL = 'https://iabp.apl.uw.edu/Data_Products/Daily_Full_Res_Data'
IABP_TABLE_URL = 'https://iabp.apl.uw.edu/IABP_Table.html'


def get_row_data(raw_row) -> List:
    """
    Create a data table row with [file_name, date, file_size] from the table in the IABP_URL index

    :param raw_row: unprocessed BS4 html object
    :return: data table row with [file_name, date, file_size]
    """
    try:
        if raw_row[1].text.endswith('.dat'):
            return [raw_row[1].text, datetime.strptime(raw_row[2].text.strip(), '%Y-%m-%d %H:%M'),
                    float(raw_row[3].text.strip().replace('K', '000').replace('M', '000000'))]
    except IndexError:
        print(raw_row)


# TODO: check if we can get a verified https request, otherwise ignore warnings
def get_buoy_data_table(today: bool = True) -> List[List]:
    """
    Get a table of all the names of buoys from the IABP database with 
    row format [file_name, datetime, file_size]
    You can download a buoy's data from URL: 'IABP_URL + file_name'

    :param today: download only the list of buoys that were uploaded today if true, otherwise download all buoys
    :return: table of all the buoys with row format [file_name, datetime, file_size]
    """
    r = requests.get(IABP_URL, verify=False)
    if r.status_code != 200:
        raise IOError(f'could not download buoy data from {r.url}')
    html = r.text
    bs = BeautifulSoup(html, features='html.parser')
    table = bs.find('table')
    rows = table.findAll('tr')
    data_table = [get_row_data(row.findAll('td')) for row in rows[3:] if get_row_data(row.findAll('td'))]
    if today:
        return [row for row in data_table if row[1].date() == date.today()]
    else:
        return data_table


def get_buoy_data_table2(today: bool = True) -> List[List]:
    """
    Get a table of all the names of buoys from the IABP database with
    row format [file_name, datetime, file_size]
    You can download a buoy's data from URL: 'IABP_URL + file_name'

    :param today: download only the list of buoys that were uploaded today if true, otherwise download all buoys
    :return: table of all the buoys with row format [file_name, datetime, file_size]
    """
    r = requests.get(IABP_TABLE_URL, verify=False)
    if r.status_code != 200:
        raise IOError(f'could not download buoy data from {r.url}')
    html = r.text
    bs = BeautifulSoup(html, features='html.parser')
    table = bs.find('table')
    rows = table.findAll('tr')
    data_table = [get_row_data(row.findAll('td')) for row in rows[3:] if get_row_data(row.findAll('td'))]
    if today:
        return [row for row in data_table if row[1].date() == date.today()]
    else:
        return data_table


def get_buoy_data(today=False) -> List[List]:
    """
    Download all buoy data from the IABP database, use today=True for updating and today=False for downloading all files
    Run from root directory to avoid creating data folders in the package.

    :param today: download only the data of buoys that were uploaded today
    :return: table of all the buoys with data that was downloaded with row format [file_name, datetime, file_size]
    """
    verify_dir(RAW_BUOY_DIR)
    buoy_table = get_buoy_data_table(today=today)
    for row in tqdm(buoy_table):
        r = requests.get(IABP_URL + row[0], verify=False)
        if r.status_code != 200:
            raise IOError(f'could not download buoy data from {r.url}')
        buoy_path = RAW_BUOY_DIR / row[0]
        if os.path.exists(buoy_path) and not today:
            os.remove(buoy_path)
        with open(buoy_path, 'w+') as buoy_file:
            buoy_file.write(r.text)
    return buoy_table


def insert_buoy_row_into_database(buoy_row, header_row):
    """
    Inserts a single buoy table row into the database
    """
    return


def load_daily_buoy_gdfs(day: date):
    """
    Load all buoy data for a given day and return as gdf
    """
    buoy_path = build_daily_path(GDF_BUOY_DIR, day, '', 'buoyfile.csv', daily_directory=True)
    df = pd.read_csv(buoy_path)
    buoy_gdf = gpd.GeoDataFrame(df, geometry=df['geometry'].apply(loads))
    buoy_gdf['datetime'] = pd.to_datetime(buoy_gdf['datetime'])
    buoy_gdf = buoy_gdf.set_index(['datetime'])
    return buoy_gdf


def load_daily_buoy_gdfs_web(day: date, hem: Hemisphere):
    """
    load a gdf of daily buoy data from hemisphere from IABP website
    
    :param: day: date for buoy data
    :hem: Hemisphere.NORTH or Hemisphere.SOUTH
    """
    datestring = day.strftime('%Y%m%d')
    hem_string = 'Arctic' if hem == Hemisphere.NORTH else 'Antarctic'
    gdf = build_daily_buoy_gdf_web(IABP_DAILY_URL + '/' + hem_string + '/FR_' + datestring + '.dat')
    return gdf


def build_buoy_gdfs(buoy_file: str):
    """
    Use raw buoy data to build a gdf for that specfic buoy
    """
    df = pd.read_csv(Path(RAW_BUOY_DIR / buoy_file), sep='\s+')
    buoy_gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.Lon, df.Lat))
    buoy_gdf['datetime'] = pd.to_datetime(buoy_gdf.apply(row_fractional_doy_to_datetime, axis=1), errors='coerce')
    buoy_gdf = buoy_gdf.set_index(['datetime'])
    buoy_gdf = buoy_gdf[buoy_gdf.index.notnull()]
    return buoy_gdf


def build_buoy_gdf_web(buoy_url: str):
    # url_data = requests.get(buoy_url, verify=False).text
    df = pd.read_csv(buoy_url, sep='\s+')
    buoy_gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.Lon, df.Lat))
    buoy_gdf['datetime'] = pd.to_datetime(buoy_gdf.apply(row_fractional_doy_to_datetime, axis=1), errors='coerce')
    buoy_gdf = buoy_gdf.set_index(['datetime'])
    buoy_gdf = buoy_gdf[buoy_gdf.index.notnull()]
    return buoy_gdf


def build_daily_buoy_gdf_web(buoy_url: str):
    columns = ['BuoyID', 'Year', 'Hour', 'Min', 'DOY', 'POS_DOY', 'Lat', 'Lon', 'BP', 'Ts', 'At']
    df = pd.read_csv(buoy_url, sep=';', names=columns)
    buoy_gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.Lon, df.Lat))
    buoy_gdf['datetime'] = pd.to_datetime(buoy_gdf.apply(row_fractional_doy_to_datetime, axis=1), errors='coerce')
    buoy_gdf = buoy_gdf.set_index(['datetime'])
    buoy_gdf = buoy_gdf[buoy_gdf.index.notnull()]
    return buoy_gdf


def build_hashable_buoy_gdf(buoy_file: str):
    """
    Builds a buoy GDF with hashable Point type
    """
    df = pd.read_csv(Path(RAW_BUOY_DIR / buoy_file), sep='\s+')
    buoy_gdf = gpd.GeoDataFrame(df, geometry=[HPoint(xy) for xy in zip(df.Lon, df.Lat)])
    buoy_gdf['datetime'] = pd.to_datetime(buoy_gdf.apply(row_fractional_doy_to_datetime, axis=1), errors='coerce')
    buoy_gdf = buoy_gdf.set_index(['datetime'])
    buoy_gdf = buoy_gdf[buoy_gdf.index.notnull()]
    return buoy_gdf


class HPoint(shapely.geometry.Point):
    '''Extends shapely Point class to be hashable
    See: https://stackoverflow.com/questions/52371968/merging-geodataframes-on-both-point-geometry-and-other-columns-at-the-same-time
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __hash__(self):
        return hash(tuple(self.coords))


def build_daily_buoy_gdfs():
    """
    After running get_buoy_data, run this to build daily csv files for all buoy data in the RAW directory
    """
    verify_dir(GDF_BUOY_DIR)
    buoy_gdf = None
    for buoy in tqdm(os.listdir(RAW_BUOY_DIR)):
        if buoy_gdf is None:
            buoy_gdf = build_buoy_gdfs(buoy)
        else:
            buoy_gdf = buoy_gdf.append(build_buoy_gdfs(buoy))
    buoy_list_by_date = [group[1] for group in buoy_gdf.groupby(buoy_gdf.index.date)]
    for daily_buoy_gdf in tqdm(buoy_list_by_date):
        dataframe_path = build_daily_path(GDF_BUOY_DIR, daily_buoy_gdf.iloc[0].name.date(),
                                          '', 'buoyfile.csv', daily_directory=True)
        daily_buoy_gdf.to_csv(str(dataframe_path))


